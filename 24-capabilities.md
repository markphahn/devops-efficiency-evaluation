---
layout: default
title: 24 Capabilities Quick Reference
---
These 24 capabilities are
defined in the book
[Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations](https://smile.amazon.com/Accelerate-Software-Performing-Technology-Organizations-ebook/dp/B07B9F83WM)


The 24 Capabilities from Gene Kim's book are classified into five categories:
* Architecture
* Product and process
* Lean management and monitoring
* Continuous delivery
* Cultural

# CONTINUOUS DELIVERY CAPABILITIES
1. Version control: Chapter 4
2. Deployment automation: Chapter 4
3. Continuous integration: Chapter 4
4. Trunk-based development: Chapter 4
5. Test automation: Chapter 4
6. Test data management: Chapter 4
7. Shift left on security: Chapter 6
8. Continuous delivery (CD): Chapter 4

# ARCHITECTURE CAPABILITIES
9. Loosely coupled architecture: Chapter 5
10. Empowered teams: Chapter 5

# PRODUCT AND PROCESS CAPABILITIES
11. Customer feedback: Chapter 8
12. Value stream: Chapter 8
13. Working in small batches: Chapter 8
14. Team experimentation: Chapter 8

# LEAN MANAGEMENT AND MONITORING CAPABILITIES
15. Change approval processes: Chapter 7
16. Monitoring: Chapter 7
17. Proactive notification: Chapter 13
18. WIP limits: Chapter 7
19. Visualizing work: Chapter 7

# CULTURAL CAPABILITIES
20. Westrum generative organizational culture: Chapter 3
21. Supporting learning: Chapter 10
22. Collaboration among teams: Chapters 3 and 5
23. Job satisfaction: Chapter 10
24. Transformational leadership: Chapter 11
