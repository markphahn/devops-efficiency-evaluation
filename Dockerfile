FROM alpine:latest
MAINTAINER Mark Hahn, mhahn@ciber.com

RUN apk update

RUN apk --no-cache add \
      jq \
      python \
      py-pip

RUN pip install --upgrade pip awscli s3cmd


