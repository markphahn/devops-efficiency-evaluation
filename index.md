---
layout: default
title: Home
---
# Introduction

The challenges of developing software centric systems are not new –
they have been with us for decades. Traditional systems were hard to
build, often complex and fragile once they are built, and are
difficult to change. In today’s digital world the rate of change
exponentially higher than it was just a few years ago. Change is
inevitable, change is constant, change is necessary.

DevOps is a collection of technologies and disciplines which the key
to enabling rapid change to software centric ecosystems large and
small. In its simplest form DevOps is the practice of bringing all
development and operations teams together. In reality its impacts are
much broader and harder to define – we believe that in the most
successful organizations the business and its customers and partners
will directly participate in the rapid evolution of software features.

# The DevOps Efficiency Matrix
This website presents the DevOps Efficiency Matrix, an open source tool
for evaluating the effectiveness of your product delivery process. The
matrix focuses on the 24 key capabilities defined in **Accelerate, The
Science of DevOps** by Nicole Forsgren PhD, Jez Humble, Gene Kim
(ISBN: 978-1942788331) which are known to drive high performing DevOps
teams.  This open framework provides a way to measure where your
organization is on its DevOps journey.

![DevOps Continuous Delivery Process](devops-infinity.png)

# Contributors

August 2019, Kirkland, Washington, USA

 * Jonathan P. Gilbert, Ph.D., [https://www.linkedin.com/in/jongilbert/](https://www.linkedin.com/in/jongilbert/)
 * Mark P. Hahn, [https://www.linkedin.com/in/markphahn/](https://www.linkedin.com/in/markphahn/)



{% for category in site.data.categories %}

# {{ category.name }}

{% for capability in category.capabilities %}

## {{ capability.index }}:  {{ capability.name }} 

<table>
<thead>
<tr><th>Level</th><th>Qualifier</th></tr>
</thead>
<tbody>
{% for level in (0..4) %}
<tr>
<td>{{ level | plus: 1 }} </td> <td>{{ capability.levels[level].qualifier }}</td>
</tr>
{% endfor %}
</tbody>
</table>


{% endfor %}
{% endfor %}


