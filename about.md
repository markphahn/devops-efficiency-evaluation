---
layout: default
title: About
---
# Contributing

Open an issue in our GitLab repository: 
`https://gitlab.com/markphahn/devops-efficiency-evaluation`

# Project ReadMe.txt

The [Project ReadMe](ReadMe.html) describes how the site is built and
maintained. Actually, it just a bunch of messy notes at the moment.

# Credits

* Nicole Forsgren PhD, Jez Humble, Gene Kim [Accelerate: The Science of Lean Software and DevOps](https://smile.amazon.com/Accelerate-Software-Performing-Technology-Organizations-ebook/dp/B07B9F83WM)
* [Mark Hahn](https://www.linkedin.com/in/markphahn/), [Ciber Gloabl LLC](https://www.ciber.com)
* Jonathan P. Gilbert, Ph.D., [https://www.linkedin.com/in/jongilbert/](https://www.linkedin.com/in/jongilbert/)
* Icons made by [Darius Dan](https://www.flaticon.com/authors/darius-dan) from [Flaticon.com](https://www.flaticon.com/)
(licensed by [Creative Commons BY 3.0](https://creativecommons.org/licenses/by/3.0))

