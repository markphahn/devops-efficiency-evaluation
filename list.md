---
layout: default
title: 24 DevOps Capabilities
---

This page is a list of the 24 capabilities identifeid in **Accelerate,
The Science of DevOps** that make up the basis for [The DevOps
Eficiency Matrix](/) on this website.

{% for category in site.data.categories %}

## {{ category.name }}

{% for capability in category.capabilities %}

<span class="capability">
[{{ capability.index }}:  {{ capability.name }}](/matrices.html#{{ capability.index }}-{{ capability.name | downcase | replace: " ", "-" }})
</span>

{% endfor %}


{% endfor %}


