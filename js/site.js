
// https://learn.jquery.com/using-jquery-core/document-ready/
$( document ).ready(function() {
    // https://learn.jquery.com/using-jquery-core/iterating/
    $("h1,h2,h3").each(function(index) {
	// https://stackoverflow.com/questions/3239598/how-can-i-get-the-id-of-an-element-using-jquery
	$(this).append('<a class="anchor-link" href="#' + $(this).attr('id') + '"> &#167; </d>');
    });
    // a better solution might be https://github.com/allejo/jekyll-anchor-headings
    

    // the below is (might) replace by a sass function in css/table @extend
    $("table").each(function(index) {
	$(this).addClass("table").addClass("table-striped").addClass("table-bordered");
    });

    $("thead").each(function(index) {
	$(this).addClass("table ").addClass("thead-dark");
    });

});
