---
layout: default
title: Efficiency Matrices
---

{% for category in site.data.categories %}
# {{ category.name }}

{% for capability in category.capabilities %}

## {{ capability.index }}:  {{ capability.name }} 

<table>
<thead>
<tr><th>Level</th><th>Qualifier</th></tr>
</thead>
<tbody>
{% for level in (0..4) %}
<tr>
<td>{{ level | plus: 1 }} </td> <td>{{ capability.levels[level].qualifier }}</td>
</tr>
{% endfor %}
</tbody>
</table>


{% endfor %}
{% endfor %}

